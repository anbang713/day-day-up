package com.study.designpattern.adapter.clazz;

import com.study.designpattern.adapter.Adaptee;
import com.study.designpattern.adapter.Target;

/**
 * 类适配器，基于继承
 *
 * @author chenganbang
 */
public class ClazzAdaptor extends Adaptee implements Target {

  @Override
  public void f1() {
    super.fa();
  }

  @Override
  public void f2() {
    // 重新实现 f2()
  }

  // 这里 fc() 不需要实现，直接继承自Adpatee类，这是跟对象适配器最大的不同点
}

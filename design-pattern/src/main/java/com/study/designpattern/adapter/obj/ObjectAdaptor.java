package com.study.designpattern.adapter.obj;

import com.study.designpattern.adapter.Adaptee;
import com.study.designpattern.adapter.Target;

/**
 * 对象适配器，基于组合
 *
 * @author chenganbang
 */
public class ObjectAdaptor implements Target {

  private final Adaptee adaptee;

  public ObjectAdaptor(Adaptee adaptee) {
    this.adaptee = adaptee;
  }

  @Override
  public void f1() {
    this.adaptee.fa();
  }

  @Override
  public void f2() {
    // 重新实现 f2()
  }

  @Override
  public void fc() {
    this.adaptee.fc();
  }
}

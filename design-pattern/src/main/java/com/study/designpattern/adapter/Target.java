package com.study.designpattern.adapter;

/**
 * 目标接口
 *
 * @author chenganbang
 */
public interface Target {

  void f1();

  void f2();

  void fc();
}

package com.study.designpattern.adapter;

/**
 * 当前实现
 *
 * @author chenganbang
 */
public class Adaptee {

  public void fa() {
  }

  public void fb() {
  }

  public void fc() {
  }
}

package com.study.designpattern.bridge;

/**
 * 短信消息发送器
 *
 * @author chenganbang
 */
public class SmsMsgSender implements MsgSender {

  @Override
  public void send(String message) {
    // 发送消息到指定的手机号
  }
}

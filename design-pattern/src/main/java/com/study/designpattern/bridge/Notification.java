package com.study.designpattern.bridge;

import cn.hutool.core.collection.CollectionUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 通知
 *
 * @author chenganbang
 */
public abstract class Notification {

  private final List<MsgSender> msgSenders = new ArrayList<>();

  public Notification(List<MsgSender> msgSenders) {
    if (CollectionUtil.isNotEmpty(msgSenders)) {
      this.msgSenders.addAll(msgSenders);
    }
  }

  public List<MsgSender> getMsgSenders() {
    return Collections.unmodifiableList(msgSenders);
  }

  /**
   * 发送消息
   *
   * @param message 消息
   */
  public abstract void notify(String message);
}

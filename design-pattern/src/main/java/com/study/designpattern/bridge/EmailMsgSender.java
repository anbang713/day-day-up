package com.study.designpattern.bridge;

/**
 * 邮箱消息发送器
 *
 * @author chenganbang
 */
public class EmailMsgSender implements MsgSender {

  @Override
  public void send(String message) {
    // 发送消息到指定的邮箱
  }
}

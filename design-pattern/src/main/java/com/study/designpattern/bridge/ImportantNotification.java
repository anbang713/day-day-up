package com.study.designpattern.bridge;

import java.util.List;

/**
 * 重要通知
 *
 * @author chenganbang
 */
public class ImportantNotification extends Notification {

  public ImportantNotification(List<MsgSender> msgSenders) {
    super(msgSenders);
  }

  @Override
  public void notify(String message) {
    for (MsgSender msgSender : getMsgSenders()) {
      msgSender.send(message);
    }
  }
}

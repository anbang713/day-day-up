package com.study.designpattern.bridge;

import java.util.List;

/**
 * 紧急消息
 *
 * @author chenganbang
 */
public class UrgencyNotification extends Notification {

  public UrgencyNotification(List<MsgSender> msgSenders) {
    super(msgSenders);
  }

  @Override
  public void notify(String message) {
    for (MsgSender msgSender : getMsgSenders()) {
      msgSender.send(message);
    }
  }
}

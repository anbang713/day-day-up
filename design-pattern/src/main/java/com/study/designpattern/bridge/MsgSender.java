package com.study.designpattern.bridge;

/**
 * 消息发送器
 *
 * @author chenganbang
 */
public interface MsgSender {

  /**
   * 发送消息
   *
   * @param message 消息
   */
  void send(String message);
}

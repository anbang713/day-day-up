package com.study.designpattern.bridge;

/**
 * 通知级别
 *
 * @author chenganbang
 */
public enum NotificationLevel {

  /**
   * 普通
   */
  NORMAL,

  /**
   * 紧急
   */
  URGENCY,

  /**
   * 重要
   */
  IMPORTANT;
}

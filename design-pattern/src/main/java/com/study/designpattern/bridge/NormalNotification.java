package com.study.designpattern.bridge;

import java.util.List;

/**
 * 普通消息通知
 *
 * @author chenganbang
 */
public class NormalNotification extends Notification {

  public NormalNotification(List<MsgSender> msgSenders) {
    super(msgSenders);
  }

  @Override
  public void notify(String message) {
    for (MsgSender msgSender : getMsgSenders()) {
      msgSender.send(message);
    }
  }
}

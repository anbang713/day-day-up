/**
 * 版权所有(C)，上海海鼎信息科技有限公司，2019~2021，所有权利保留。
 * <p>
 * 项目名：	day-day-up 文件名： 模块说明： 修改历史： 2021/8/9 - chenganbang - 创建。
 */
package com.study.designpattern.singleton;

/**
 * 懒汉式（静态内部类）
 *
 * @author chenganbang
 */
public class StaticInnerClassSingleton {

  private StaticInnerClassSingleton() {
  }

  /**
   * <p>
   * 实例由内部类创建，由于 JVM 在加载外部类的过程中, 是不会加载静态内部类的, 只有内部类的属性/方法被调用时才会被加载, 并初始化其静态属性。静态属性由于被
   * `static`修饰，保证只被实例化一次，并且严格保证实例化顺序。
   * <p/>
   *
   * <p>
   * 通过静态内部类的方式实现单例，在没有加任何锁的情况下，保证了多线程下的安全，并且没有任何性能影响和空间的浪费。可以说是很优秀了。
   * </p>
   */
  private static class SingletonHolder {
    private static final StaticInnerClassSingleton INSTANCE = new StaticInnerClassSingleton();
  }

  public static StaticInnerClassSingleton getInstance() {
    return SingletonHolder.INSTANCE;
  }

  public static void main(String[] args) {
    StaticInnerClassSingleton obj1 = StaticInnerClassSingleton.getInstance();
    StaticInnerClassSingleton obj2 = StaticInnerClassSingleton.getInstance();

    System.out.println(obj1 == obj2);// 输出：true
  }
}
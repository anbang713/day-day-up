package com.study.designpattern.singleton;

/**
 * 懒汉式：类加载不会导致该单实例对象被创建，而是首次使用该对象时才会创建。
 *
 * @author Anbang713
 */
public class LazySingleton {

  /**
   * 毫无疑问，管它饿汉还是懒汉，构造方法都应该是私有的。
   */
  private LazySingleton() {
  }

  /**
   * 静态变量，注意是静态变量。
   */
  private static LazySingleton instance;

  /**
   * 使用synchronized加锁。啥也不说，反正你调我，就先加锁。
   * <p>
   * 拓展：锁都能保证现在安全，但也降低性能。
   * <p/>
   *
   * @return 类的唯一对象
   */
  public static synchronized LazySingleton getInstance() {
    if (instance == null) {
      instance = new LazySingleton();
    }
    return instance;
  }

  public static void main(String[] args) {
    LazySingleton obj1 = LazySingleton.getInstance();
    LazySingleton obj2 = LazySingleton.getInstance();

    System.out.println(obj1 == obj2);// 输出：true
  }
}

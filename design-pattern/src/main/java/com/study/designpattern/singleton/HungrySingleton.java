package com.study.designpattern.singleton;

/**
 * 饿汉式：类加载就会导致该单实例对象被创建。
 *
 * @author Anbang713
 */
public class HungrySingleton {

  /**
   * 所有的单例类，构造方法都应该是private。避免外界使用new关键字创建类对象。
   */
  private HungrySingleton() {
  }

  /**
   * 饿汉式指的是声明一个类的静态常量，并直接创建一个对象出来。
   * <p>
   * 扩展：
   * 1、类加载总共分为：加载、链接（验证、准备、解析）、初始化几个过程。
   * 2、类的静态常量（static final）在编译时赋予零值，在链接（准备）阶段完成初始化。
   * 3、类的静态变量（static）在链接（准备）阶段赋予零值，在初始化阶段完成赋值。
   * </p>
   * 所以我们说饿汉式的单例，是在类加载的时候就会对象就会被创建出来。如果该对象足够大的话，而一直没有使用就会造成内存的浪费。
   */
  private static final HungrySingleton INSTANCE = new HungrySingleton();

  /**
   * 提供一个静态的方法，用于获取唯一的一个对象
   *
   * @return 类的唯一对象
   */
  public static HungrySingleton getInstance() {
    return INSTANCE;
  }

  public static void main(String[] args) {
    HungrySingleton obj1 = HungrySingleton.getInstance();
    HungrySingleton obj2 = HungrySingleton.getInstance();

    System.out.println(obj1 == obj2); // 输出：true
  }
}

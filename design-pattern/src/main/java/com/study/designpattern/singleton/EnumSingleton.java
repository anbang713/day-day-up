/**
 * 版权所有(C)，上海海鼎信息科技有限公司，2019~2021，所有权利保留。
 * <p>
 * 项目名：	day-day-up 文件名： 模块说明： 修改历史： 2021/8/9 - chenganbang - 创建。
 */
package com.study.designpattern.singleton;

/**
 * 饿汉式（枚举）
 *
 * <p>
 * 枚举类实现单例模式是极力推荐的单例实现模式，因为枚举类型是线程安全的，并且只会装载一次，
 * 设计者充分的利用了枚举的这个特性来实现单例模式，枚举的写法非常简单，而且枚举类型是所用单例实现中唯一一种不会被破坏的单例实现模式。
 * </P>
 *
 * @author chenganbang
 */
public enum EnumSingleton {

  ONE, TWO;
}
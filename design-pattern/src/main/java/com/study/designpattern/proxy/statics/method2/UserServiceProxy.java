package com.study.designpattern.proxy.statics.method2;

import com.study.designpattern.proxy.statics.UserService;

/**
 * 代理类
 *
 * @author chenganbang
 */
public class UserServiceProxy implements UserService {

  private final UserService userService;

  public UserServiceProxy(UserService userService) {
    this.userService = userService;
  }

  @Override
  public void login(String userName, String password) {
    beforeLogin();

    // 委托给 userService 处理。
    this.userService.login(userName, password);

    afterLogin();
  }

  private void beforeLogin() {
    // do something
  }

  private void afterLogin() {
    // do something
  }
}

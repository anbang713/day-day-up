package com.study.designpattern.proxy.statics.method1;

import com.study.designpattern.proxy.statics.UserService;
import com.study.designpattern.proxy.statics.UserServiceImpl;

/**
 * 代理类
 *
 * @author chenganbang
 */
public class UserServiceProxy extends UserServiceImpl implements UserService {

  @Override
  public void login(String userName, String password) {
    beforeLogin();

    super.login(userName, password);

    afterLogin();
  }

  private void beforeLogin() {
    // do something
  }

  private void afterLogin() {
    // do something
  }
}

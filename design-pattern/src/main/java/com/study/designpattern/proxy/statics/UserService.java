package com.study.designpattern.proxy.statics;

/**
 * 代理接口
 *
 * @author chenganbang
 */
public interface UserService {

  /**
   * 用户名密码登录
   *
   * @param userName 用户名，不允许为空
   * @param password 密码，不允许为空
   */
  void login(String userName, String password);
}

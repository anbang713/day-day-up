package com.study.designpattern.proxy.statics;

/**
 * 被代理类
 *
 * @author chenganbang
 */
public class UserServiceImpl implements UserService {

  @Override
  public void login(String userName, String password) {
    // do something
  }
}

package com.study.algorithm.sort;

import org.junit.Test;

/**
 * 冒泡排序单元测试
 *
 * @author chenganbang
 */
public class BubbleTest {

  @Test
  public void sortTest() {
    Integer[] a = { 4, 5, 6, 3, 2, 1 };
    Bubble.getInstance().sort(a);
  }
}
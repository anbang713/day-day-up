package com.study.algorithm.sort;

import org.junit.Test;

/**
 * 归并排序单元测试
 *
 * @author YangGuGu
 */
public class MergeTest {

  @Test
  public void sortTest() {
    Integer[] a = { 8, 1, 2, 5, 7, 4, 6, 3 };
    Merge.getInstance().sort(a);
  }
}

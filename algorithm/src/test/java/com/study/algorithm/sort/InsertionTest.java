package com.study.algorithm.sort;

import org.junit.Test;

/**
 * 插入排序单元测试
 *
 * @author chenganbang
 */
public class InsertionTest {

  @Test
  public void sortTest() {
    Integer[] a = { 9, 1, 2, 5, 7, 4, 8, 6, 3 };
    Insertion.getInstance().sort(a);
  }
}
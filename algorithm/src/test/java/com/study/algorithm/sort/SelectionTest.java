package com.study.algorithm.sort;

import org.junit.Test;

/**
 * 选择排序单元测试
 *
 * @author chenganbang
 */
public class SelectionTest {

  @Test
  public void sortTest() {
    Integer[] a = { 4, 6, 8, 7, 9, 2, 10, 1 };
    Selection.getInstance().sort(a);
  }
}
package com.study.algorithm.sort;

import org.junit.Test;

/**
 * 希尔排序单元测试
 *
 * @author chenganbang
 */
public class ShellTest {

  @Test
  public void sortTest() {
    Integer[] a = { 9, 1, 2, 5, 7, 4, 8, 6, 3 };
    Shell.getInstance().sort(a);
  }
}
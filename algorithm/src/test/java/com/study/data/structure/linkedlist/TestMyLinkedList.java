package com.study.data.structure.linkedlist;

import org.junit.Test;

/**
 * 单链表测试类
 *
 * @author YangGuGu
 */
public class TestMyLinkedList {

  @Test
  public void test() {
    MyLinkedList<Integer> linkedList = new MyLinkedList<>();
    linkedList.addToHead(1);
    linkedList.addToHead(2);
    linkedList.addToHead(3);
    linkedList.addToHead(4);
    linkedList.addToHead(5);
    linkedList.print();

    linkedList.addToTail(6);
    linkedList.print();

    linkedList.addAtIndex(6, 7);
    linkedList.print();

    linkedList.deleteAtIndex(0);
    linkedList.print();

    Integer integer = linkedList.get(5);
    System.out.println("值：" + integer);
  }
}

package com.study.algorithm.sort;

/**
 * 排序接口
 *
 * @author YangGuGu
 */
public interface Sort {

  /**
   * 升序排序
   *
   * @param source
   *     原数据
   */
  void sort(Comparable[] source);
}

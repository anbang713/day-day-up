package com.study.algorithm.sort;

import java.text.MessageFormat;
import java.util.Arrays;

/**
 * 排序协助类，使用静态内部类实现单例
 *
 * @author YangGuGu
 */
public class SortHelper {

  private SortHelper() {
  }

  private static class Holder {
    private static final SortHelper INSTANCE = new SortHelper();
  }

  public static SortHelper getInstance() {
    return Holder.INSTANCE;
  }

  /**
   * a 是否大于 b
   *
   * @return true表示大于；false表示等于或小于
   */
  public boolean greater(Comparable a, Comparable b) {
    if (a == null) {
      return true;
    }
    if (b == null) {
      return false;
    }
    return a.compareTo(b) > 0;
  }

  /**
   * 交换数组 array 下标 i 和 j 位置的值
   *
   * @param array
   *     数组
   * @param i
   *     下标 i，不合法的下标值拒绝排序
   * @param j
   *     下标 j，不合法的下标值拒绝排序
   */
  public void exchange(Comparable[] array, int i, int j) {
    if (array == null) {
      return;
    }

    int arraySize = array.length;
    if (i < 0 || i >= arraySize) {
      return;
    }
    if (j < 0 || j >= arraySize) {
      return;
    }

    // 开始交换
    Comparable temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }

  /**
   * 打印数组
   *
   * @param index
   *     第 index 次
   * @param array
   *     第 index 次排序后的数组
   */
  public void printArray(int index, Comparable[] array) {
    System.out.println(
        MessageFormat.format("第 {0} 趟排序结果：{1}", index, Arrays.toString(array)));
  }
}

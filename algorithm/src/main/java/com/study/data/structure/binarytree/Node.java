package com.study.data.structure.binarytree;

import java.io.Serializable;
import java.util.Comparator;

/**
 * 二叉树节点
 *
 * @author YangGuGu
 */
public class Node<K, V> implements Serializable {
  private K key;
  private V value;
  private Node<K, V> left;
  private Node<K, V> right;

  public Node() {
  }

  public Node(K key, V value) {
    this.key = key;
    this.value = value;
  }

  public Node(K key, V value, Node<K, V> left, Node<K, V> right) {
    this.key = key;
    this.value = value;
    this.left = left;
    this.right = right;
  }

  /**
   * 节点的key
   */
  public K getKey() {
    return key;
  }

  /**
   * 节点的value
   */
  public V getValue() {
    return value;
  }

  public void setValue(V value) {
    this.value = value;
  }

  /**
   * 左节点
   */
  public Node<K, V> getLeft() {
    return left;
  }

  public void setLeft(Node<K, V> left) {
    this.left = left;
  }

  /**
   * 右节点
   */
  public Node<K, V> getRight() {
    return right;
  }

  public void setRight(Node<K, V> right) {
    this.right = right;
  }
}

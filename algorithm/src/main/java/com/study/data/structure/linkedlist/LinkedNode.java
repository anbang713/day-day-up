package com.study.data.structure.linkedlist;

/**
 * 链表节点
 *
 * @author YangGuGu
 */
public class LinkedNode<T extends Comparable<T>> {

  private T value;
  private LinkedNode<T> next;

  public LinkedNode() {

  }

  public LinkedNode(T value) {
    if (value == null) {
      throw new IllegalArgumentException("value is not allow null");
    }
    this.value = value;
  }

  /**
   * 节点的值
   */
  public T getValue() {
    return value;
  }

  /**
   * 下个节点的引用
   */
  public LinkedNode<T> getNext() {
    return next;
  }

  public void setNext(LinkedNode<T> next) {
    this.next = next;
  }

  @Override
  public String toString() {
    if (value == null) {
      return "value is null";
    }
    return value.toString();
  }
}
